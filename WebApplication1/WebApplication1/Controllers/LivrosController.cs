﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{	/// <summary>
	/// LivrosController
	/// </summary>
    public class LivrosController : ApiController
    {
		/// <summary>
		/// GET
		/// </summary>
		/// <returns></returns>
        // GET: api/Livros
        public List<Livro> Get()
        {
			return Biblioteca.Livros;
        }

		/// <summary>
		/// Err Codes
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
        // GET: api/Livros/5
        public IHttpActionResult Get(int id)
        {
			Livro livro = Biblioteca.Livros.FirstOrDefault(x => x.Id == id);

			if (livro != null)
			{
				return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, livro));

			}

			return ResponseMessage(Request.CreateResponse(HttpStatusCode.NotFound, "O Livro Não Foi Encontrado"));

		}



		/// <summary>
		/// POST
		/// </summary>
		/// <param name="obj"></param>
		/// <returns></returns>
        // POST: api/Livros
        public IHttpActionResult Post([FromBody]Livro obj)
        {
			Livro livro = Biblioteca.Livros.FirstOrDefault(x => x.Id == obj.Id);

			if (livro != null)
			{
				return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.Conflict, "Já Existe Esse ID"));
			}
			Biblioteca.Livros.Add(obj);

			return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.OK, "Inserido!"));

		}


		/// <summary>
		/// PUT
		/// </summary>
		/// <param name="obj"></param>
		/// <returns></returns>
		// PUT: api/Livros/5
		public IHttpActionResult Put([FromBody]Livro obj)
        {
			Livro livro = Biblioteca.Livros.FirstOrDefault(x => x.Id == obj.Id);

			if (livro != null)
			{
				livro.Titulo = obj.Titulo;
				livro.Autor = obj.Autor;
				return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.OK, "Atualizado!"));
			}

			return ResponseMessage(Request.CreateResponse(HttpStatusCode.NotFound, "Não Existe Nenhum Livro Com Esse ID"));
		}

		/// <summary>
		/// DELETE
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		// DELETE: api/Livros/5
		public IHttpActionResult Delete(int id)
        {
			Livro livro = Biblioteca.Livros.FirstOrDefault(x => x.Id == id);

			if (livro != null)
			{
				Biblioteca.Livros.Remove(livro);
				return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.OK, "Removido!"));
			}

			return ResponseMessage(Request.CreateResponse(HttpStatusCode.NotFound, "Não Existe Nenhum Livro Com Esse ID"));
		}
	}
}
