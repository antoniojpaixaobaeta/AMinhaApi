﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class EmpregadosController : ApiController
    {
		private List<Empregado> Funcionarios;

		public EmpregadosController()
		{

			Funcionarios = new List<Empregado>
			{
				new Empregado {Id = 1, Nome = "Manel", Apelido = "Luz" },
				new Empregado {Id = 2, Nome = "Juana", Apelido = "Pitxa" },
				new Empregado {Id = 3, Nome = "Manuela", Apelido = "Alexandra" },
			};

		}
		/// <summary>
		/// GET - Todos os Empregados
		/// </summary>
		/// <returns></returns>
		// GET: api/Empregados
		public List<Empregado> Get()
        {
            return Funcionarios;
        }

		/// <summary>
		/// GET - Empregado por Nome
		/// </summary>
		/// <returns></returns>
		[Route("api/Empregados/GetNomes")]
		// GET: api/Empregados/GetNomes
		public List<string> GetNomes()
		{
			List<string> output = new List<string>();

			foreach (var e in Funcionarios)
			{
				output.Add(e.Nome);
			}

			return output;
		}

		/// <summary>
		/// GET - Empregado por ID
		/// </summary>
		/// <param name="id">ID Empregado</param>
		/// <returns></returns>
		// GET: api/Empregados/5
		public Empregado Get(int id)
        {
            return Funcionarios.FirstOrDefault(x => x.Id == id);
        }

		/// <summary>
		/// POST - Empregado
		/// </summary>
		/// <param name="valor">Nome Empregado</param>
        // POST: api/Empregados
        public void Post([FromBody]Empregado valor)
        {
			Funcionarios.Add(valor);

        }

		/// <summary>
		/// PUT - Empregado
		/// </summary>
		/// <param name="id"></param>
		/// <param name="value"></param>
        // PUT: api/Empregados/5
        public void Put(int id, [FromBody]string value)
        {
        }

		/// <summary>
		/// DELETE - Empregado
		/// </summary>
		/// <param name="id"></param>
        // DELETE: api/Empregados/5
        public void Delete(int id)
        {
        }
    }
}
