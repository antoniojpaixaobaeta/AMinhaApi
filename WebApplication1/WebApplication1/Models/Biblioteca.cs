﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
	public class Biblioteca
	{
		private static List<Livro> livros;

		public static List<Livro> Livros
		{
			get
			{
				if (livros == null)
				{
					GerarLivros();
				}

				return livros;
			}
			set
			{
				livros = value;
			}
		}

		private static void GerarLivros()
		{
			livros = new List<Livro>();

			livros.Add(new Livro
			{
				Id = 1,
				Titulo = "O CET 30",
				Autor = "Amadeu",

			});

			livros.Add(new Livro
			{
				Id = 2,
				Titulo = "Oi Amigos",
				Autor = "Ricardo",

			});

			livros.Add(new Livro
			{
				Id = 1,
				Titulo = "O Cinel",
				Autor = "Voçê",

			});
		}
	}
}